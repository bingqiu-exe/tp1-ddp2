import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
public class Gameover extends World
{

    /**
     * Constructor for objects of class Gameover.
     * 
     */
    public Gameover()
    {    
        super(600, 400, 1); 
        gameoverScreen();
    }
    
    public void gameoverScreen()
    {
        GreenfootImage image1 = new GreenfootImage("GAME OVER", 60, Color.BLACK, null);
        getBackground().drawImage(image1, 300-image1.getWidth()/2, 100-image1.getHeight()/2);
        GreenfootImage image = new GreenfootImage("Start Again", 40, Color.BLUE, null);
        getBackground().drawImage(image, 300-image.getWidth()/2, 320-image.getHeight()/2);
    }
    
    public void act()
    {
        if (Greenfoot.isKeyDown("space"))
        {
            Greenfoot.setWorld(new Level1());
        }
    }
}
