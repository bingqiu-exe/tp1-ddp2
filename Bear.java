import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Bear here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Bear extends Creature
{
    /**
     * Act - do whatever the Bear wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    
    Level1 easyMode;
    Level2 intermediateMode;
    Level3 hardMode;
    private int bananasEaten = 0;
    private int snakesEaten = 0;
    private int poisonEaten = 0;
    private int pearlEaten = 0;
    private GreenfootImage image1 = new GreenfootImage("wombat.png");
    private int sizer;
    
    public void act()
    {
        lookForBanana();
        lookForSnake();
        lookForPoison();
        lookForPearl();
        checkKeyPress();
    }
    
    public void lookForBanana()
    {
        if (canSee(Banana.class))
            {
                eat(Banana.class);
                Greenfoot.playSound("slurp.wav");
                bananasEaten = bananasEaten + 1;
                if (bananasEaten > 0)
                {
                    GreenfootImage img = new GreenfootImage(image1);
                    img.scale(img.getWidth()*(10+(++sizer))/10, img.getHeight()*(10+sizer)/10);
                    setImage(img);
                }
                easyMode.score++;
                    if (bananasEaten == 3)
                    {
                        fanfareSound();
                    }
                intermediateMode.score++;
                    if (bananasEaten == 8)
                    {
                        fanfareSound();
                    }
                hardMode.score++;
                    if(bananasEaten == 30)
                    {
                        fanfareSound();
                    }
                lookForPearl();
            }
    }
    
    public void fanfareSound()
    {
        Greenfoot.playSound("fanfare.wav");
    }
    
    public void ouchSound()
    {
        Greenfoot.playSound("au.wav");
    }
    
    public void lookForSnake()
    {
        if (canSee(Snake.class))
            {
                eat(Snake.class);
                ouchSound();
                snakesEaten = snakesEaten + 1;
                easyMode.score--;
                intermediateMode.score--;
                hardMode.score--;
                if (easyMode.score < 0)
                {
                    Greenfoot.stop();
                    Greenfoot.setWorld(new Gameover());
                }
                if (intermediateMode.score < 0)
                {
                    Greenfoot.stop();
                    Greenfoot.setWorld(new Gameover());
                }
                if (hardMode.score < 0)
                {
                    Greenfoot.stop();
                    Greenfoot.setWorld(new Gameover());
                }
            }
    }
    
    public void lookForPoison()
    {
        if (canSee(Poison.class))
        {
            eat(Poison.class);
            ouchSound();
            Greenfoot.setWorld(new Gameover());
            easyMode.score = 0;
            intermediateMode.score = easyMode.score;
            hardMode.score = intermediateMode.score;
        }
    }
    
    public void lookForPearl()
    {
        if (canSee(Pearl.class))
        {
            eat(Pearl.class);
            Greenfoot.playSound("slurp.wav");
            pearlEaten = pearlEaten + 1;
            if (pearlEaten == 1)
            {
                hardMode.score *= 2;
            }
        }
    }
    
    public void checkKeyPress()
    {
        int speed = 0;
        
        if (Greenfoot.isKeyDown("up"))
            {
                setDirection(NORTH);
                setLocation(getX(), getY()-1);
                move(speed);
                if (Greenfoot.isKeyDown("space"))
                    {
                        move(2*(speed+1));
                    }
            }
        if (Greenfoot.isKeyDown("left"))
            {
                setLocation(getX()-1, getY());
                move(speed);
                if (Greenfoot.isKeyDown("space"))
                    {
                        move(2*speed-1);
                    }
            }
        if (Greenfoot.isKeyDown("right"))
            {
                setDirection(EAST);
                setLocation(getX()+1, getY());
                move(speed);
                if (Greenfoot.isKeyDown("space"))
                    {
                        move(2*speed+1);
                    }
            }
        if (Greenfoot.isKeyDown("down"))
            {
                setDirection(SOUTH);
                setLocation(getX(), getY()+1);
                move(speed);
                if (Greenfoot.isKeyDown("space"))
                {
                    move(2*(speed+1));
                }
            }
    }
}
