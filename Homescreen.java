import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
public class Homescreen extends World
{

    /**
     * Constructor for objects of class Homescreen.
     * 
     */
    private String text = "THE HUNGRY BEAR";
    
    public Homescreen()
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(600, 400, 1);
        Greenfoot.start();
        titleScreen();
        addObject(new Bear(), 250, 250);
        addObject(new Banana(), 350, 250);
    }

    public void titleScreen()
    {
        GreenfootImage image = new GreenfootImage("The Hungry Bear", 60, Color.BLACK, null);
        getBackground().drawImage(image, 300-image.getWidth()/2, 100-image.getHeight()/2);
    }
    
    public void act()
    {
        GreenfootImage image = new GreenfootImage("Mulai", 40, Color.RED, null);
        getBackground().drawImage(image, 300-image.getWidth()/2, 320-image.getHeight()/2);
        if (Greenfoot.isKeyDown("space")) 
        {
            Greenfoot.setWorld(new Level1());
        }
    }
}
