import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
public class Poison extends Creature
{
    /**
     * Act - do whatever the Poison wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    Level1 easyMode;
    Level2 intermediateMode;
    
    public void act()
    {
        LookForBear();
    }
    public void LookForBear()
    {
        if (canSee(Bear.class))
            {
                eat(Bear.class);
                Greenfoot.setWorld(new Gameover());
                easyMode.score = 0;
                intermediateMode.score = easyMode.score;
            }
    }
}
