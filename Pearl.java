import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
public class Pearl extends Creature
{
    /**
     * Act - do whatever the Pearl wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    private GreenfootImage image2 = new GreenfootImage("gold-ball.png");
    private int sizer;
    private int direction = 0;
    
    public void act()
    {
        imgSizer();
        randomMove();
    }
    
    public void imgSizer()
    {
        GreenfootImage img = new GreenfootImage(image2);
        img.scale((img.getWidth()/4), (img.getHeight()/4));
        setImage(img);
    }
    
    public void randomMove()
    {
        move(2);
        if (Greenfoot.getRandomNumber(100) < 10)
        {
            turn(Greenfoot.getRandomNumber(90) - 45);
        }
    }
}
