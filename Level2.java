import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
public class Level2 extends World
{
    /**
     * Constructor for objects of class Level2.
     * 
     */
    public static int score = 0;
    public Level2()
    {    
        super(600, 400, 1);
        addObject(new Bear(), 250, 250);
        addObject(new Banana(), 200, 300);
        addObject(new Banana(), 400, 100);
        addObject(new Banana(), 350, 270);
        addObject(new Banana(), 450, 300);
        addObject(new Banana(), 550, 150);
        addObject(new Snake(), 500, 200);
        addObject(new Snake(), 380, 340);
        addObject(new Snake(), 410, 290);
        addObject(new Poison(), 100, 250);
        addObject(new Poison(), 300, 220);
    }
    
    public void act()
    {
        showText("score: " + score, 50, 25);
        showText("Level 2", 550, 25);
        if (score >= 8)
        {
            Greenfoot.setWorld(new Level3());
        }
    }
}
