import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Winscreen here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Winscreen extends World
{

    /**
     * Constructor for objects of class Winscreen.
     * 
     */
    public Winscreen()
    {    
        super(600, 400, 1); 
        winScreen();
    }
    
    public void winScreen()
    {
        GreenfootImage image1 = new GreenfootImage("CONGRATULATIONS", 60, Color.BLACK, null);
        getBackground().drawImage(image1, 300-image1.getWidth()/2, 100-image1.getHeight()/2);
        GreenfootImage image2 = new GreenfootImage("You Win", 60, Color.BLACK, null);
        getBackground().drawImage(image2, 300-image2.getWidth()/2, 160-image2.getHeight()/2);
        GreenfootImage image = new GreenfootImage("Play Again", 40, Color.BLUE, null);
        getBackground().drawImage(image, 300-image.getWidth()/2, 320-image.getHeight()/2);
    }
    
    public void act()
    {
        if (Greenfoot.isKeyDown("space"))
        {
            Greenfoot.stop();
            Greenfoot.setWorld(new Homescreen());
        }
    }
}
