import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
public class Level1 extends World
{
    /**
     * Constructor for objects of class MyWorld.
     * 
     */
    public static int score = 0;
    public Level1()
    {    
        super(600, 400, 1);
        addObject(new Bear(), 250, 250);
        addObject(new Banana(), 100, 300);
        addObject(new Banana(), 450, 210);
        addObject(new Banana(), 300, 200);
        addObject(new Snake(), 170, 350);
        addObject(new Snake(), 550, 150);
    }
    
    public void act()
    {
        showText("score: " + score, 50, 25);
        showText("Level 1", 550, 25);
        if (score >= 3)
        {
            Greenfoot.setWorld(new Level2());
        }
    }
}
