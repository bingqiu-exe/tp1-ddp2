import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
public class Level3 extends World
{
    public static int score = 0;
    /**
     * Constructor for objects of class Level3.
     * 
     */
    public Level3()
    {    
        super(600, 400, 1);
        addObject(new Bear(), 250, 250);
        addObject(new Banana(), 100, 200);
        addObject(new Banana(), 400, 100);
        addObject(new Banana(), 450, 300);
        addObject(new Banana(), 410, 290);
        addObject(new Banana(), 300, 200);
        addObject(new Banana(), 550, 150);
        addObject(new Banana(), 200, 300);
        addObject(new Snake(), 170, 350);
        addObject(new Snake(), 540, 50);
        addObject(new Snake(), 70, 80);
        addObject(new Snake(), 480, 200);
        addObject(new Poison(), 200, 200);
        addObject(new Poison(), 100, 310);
        addObject(new Poison(), 100, 100);        
        addObject(new Pearl(), 400, 250);
    }
    
    public void act()
    {
        showText("score: " + score, 50, 25);
        showText("Level 3", 550, 25);
        if (score == 30)
        {
            score = 0;
            Greenfoot.setWorld(new Winscreen());
        }
    }
}
